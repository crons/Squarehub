//+---------------+---------------+-----------------
// App: Javascript file
// @    Prasanna.Prakash               04/2015
//+---------------+---------------+-----------------

//+---------------+---------------+-----------------
// Module dependencies.
// @    Prasanna.Prakash               04/2015
//+---------------+---------------+-----------------
var express         = require('express');
var path            = require('path');
var favicon         = require('serve-favicon');
var logger          = require('morgan');
var cookieParser    = require('cookie-parser');
var bodyParser      = require('body-parser');
var mongoose        = require('mongoose');
var passport        = require('passport');
var LocalStrategy   = require('passport-local').Strategy;

var routes          = require('./routes/index').router;

var app             = express();

//+---------------+---------------+-----------------
// Module dependencies.
// @    Prasanna.Prakash               04/2015
//+---------------+---------------+-----------------
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('express-session')
    ({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
    }));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);

// passport config
var Account = require('./models/account');
passport.use(new LocalStrategy(Account.authenticate()));
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());

var dbConfig = require('./routes/db.js');
mongoose.connect(dbConfig.url);

//+---------------+---------------+-----------------
// catch 404 and forward to error handler
// @    Prasanna.Prakash               04/2015
//+---------------+---------------+-----------------
app.use(function(req, res, next)
    {
    var err     = new Error('Not Found');
    err.status  = 404;
    next(err);
    });

//+---------------+---------------+-----------------
// Error handlers
// @    Prasanna.Prakash               04/2015
//+---------------+---------------+-----------------
if (app.get('env') === 'development')
    {
    app.use(function(err, req, res, next)
        {
        res.status(err.status || 500);
        res.render('error',
            {
            message: err.message,
            error: err
            });
        });
    }

app.use(function(err, req, res, next)
    {
    res.status(err.status || 500);
    res.render('error',
        {
        message: err.message,
        error: {}
        });
    });

module.exports = app;
