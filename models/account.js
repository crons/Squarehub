//+---------------+---------------+-----------------
// Acoount: Javascript file
// @ Prasanna.Prakash                   08/2016
//+---------------+---------------+-----------------

//+---------------+---------------+-----------------
// Module dependencies.
// @ Prasanna.Prakash                   08/2016
//+---------------+---------------+-----------------
var mongoose                = require('mongoose');
var Schema                  = mongoose.Schema;
var passportLocalMongoose   = require('passport-local-mongoose');

//+---------------+---------------+-----------------
// Layout of the schema
// @ Prasanna.Prakash                   08/2016
//+---------------+---------------+-----------------
var Account = new Schema
    ({
    username: String,
    password: String,
    email   : String,
    first   : String,
    last    : String
    });

Account.plugin(passportLocalMongoose);

module.exports = mongoose.model('Account', Account);
