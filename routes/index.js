//+---------------+---------------+-----------------
// App: Javascript file
// @ Prasanna.Prakash                   08/2016
//+---------------+---------------+-----------------

//+---------------+---------------+-----------------
// Module dependencies.
// @ Prasanna.Prakash                   08/2016
//+---------------+---------------+-----------------
var express     = require('express');
var passport    = require('passport');
var Account     = require('../models/account');
var router      = express.Router();
//+---------------+---------------+-----------------
// Render to the local jade files
// @ Prasanna.Prakash                   08/2016
//+---------------+---------------+-----------------
router.get('/',
    function (req, res)
        {
        res.render('index', { user : req.user });
        });

router.get('/register',
    function(req, res)
        {
        res.render('register', { });
        });

//+---------------+---------------+-----------------
// Account creation when registering a user
// @ Prasanna.Prakash                   08/2016
//+---------------+---------------+-----------------
router.post('/register',
    function(req, res, next)
        {
        Account.register(new Account({ username : req.body.username, email : req.body.email, first : req.body.first, last : req.body.last}), req.body.password,
            function(err, account)
                {
                if (err)
                    return res.render('register', { error : err.message });

                passport.authenticate('local')(req, res,
                    function ()
                        {
                        req.session.save(function (err)
                            {
                            if (err)
                                return next(err);

                            res.redirect('/');
                            });
                        });
                });
        });

//+---------------+---------------+-----------------
// Login: Authentication
// @ Prasanna.Prakash                   08/2016
//+---------------+---------------+-----------------
router.post('/login', passport.authenticate('local'),
    function(req, res)
        {
        res.redirect('/');
        });

//+---------------+---------------+-----------------
// Logout
// @ Prasanna.Prakash                   08/2016
//+---------------+---------------+-----------------
router.get('/logout',
    function(req, res)
        {
        req.logout();
        res.redirect('/');
        });

module.exports.router = router;
