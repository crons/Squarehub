//+---------------+---------------+-----------------
// User: Unit test
// @    Prasanna.Prakash               08/2016
//+---------------+---------------+-----------------

//+---------------+---------------+-----------------
// Module dependencies.
// @    Prasanna.Prakash               08/2016
//+---------------+---------------+-----------------
var should      = require("should");
var mongoose    = require('mongoose');
var Account     = require("../models/account.js");
var db;

describe('Account',
    function()
        {
        //+---------------+---------------+-----------------
        // Prepare test database
        // @    Prasanna.Prakash               08/2016
        //+---------------+---------------+-----------------
        before(function(done)
            {
            db = mongoose.connect('mongodb://localhost/test');
                done();
            });

        //+---------------+---------------+-----------------
        // Close test database
        // @    Prasanna.Prakash               08/2016
        //+---------------+---------------+-----------------
        after(function(done)
            {
            mongoose.connection.close();
            done();
            });

        //+---------------+---------------+-----------------
        // Populate test database with test profile
        // @    Prasanna.Prakash               08/2016
        //+---------------+---------------+-----------------
        beforeEach(function(done)
            {
            var account = new Account({username: 'test', password: 'test', email: 'test@test-domain', first : 'FirstName', last : 'LastName'});

            account.save(function(error)
                {
                if (error) console.log('error' + error.message);
                else console.log('no error');
                done();
                });
            });

        //+---------------+---------------+-----------------
        // Test for username
        // @    Prasanna.Prakash               08/2016
        //+---------------+---------------+-----------------
        it('Find a user by username',
            function(done)
                {
                Account.findOne({ username: 'test' },
                    function(err, account)
                        {
                        account.username.should.eql('test');
                        console.log("   username: ", account.username);
                        done();
                        });
                });

        //+---------------+---------------+-----------------
        // Test for email
        // @    Prasanna.Prakash               08/2016
        //+---------------+---------------+-----------------
        it('Find a user by email',
            function(done)
                {
                Account.findOne({ email: 'test@test-domain' },
                    function(err, account)
                        {
                        account.email.should.eql('test@test-domain');
                        console.log("   email: ", account.email);
                        done();
                        });
                });

        //+---------------+---------------+-----------------
        // Test for First name
        // @    Prasanna.Prakash               08/2016
        //+---------------+---------------+-----------------
        it('Find a user by first name',
            function(done)
                {
                Account.findOne({ first : 'FirstName' },
                    function(err, account)
                        {
                        account.first.should.eql('FirstName');
                        console.log("   first : ", account.first);
                        done();
                        });
                });

        //+---------------+---------------+-----------------
        // Test for last name
        // @    Prasanna.Prakash               08/2016
        //+---------------+---------------+-----------------
        it('Find a user by last name',
            function(done)
                {
                Account.findOne({ last : 'LastName' },
                    function(err, account)
                        {
                        account.last.should.eql('LastName');
                        console.log("   last : ", account.last);
                        done();
                        });
                });


        afterEach(function(done)
            {
            Account.remove({},
                function()
                {
                done();
                });
            });

        });
